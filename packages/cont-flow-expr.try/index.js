// SPDX-License-Identifier: ISC

/**
 * @template T
 * @typedef TryExpr<T>
 * @property {(fn: (error: Error) => T) => T} catch
 */

/**
 * @template T
 * @param {() => T} tryFn
 * @param {() => T} catchFn
 * @returns {T}
 */
function hctac(tryFn, catchFn) {
	try {
		return tryFn();
	} catch (error) {
		return catchFn(error);
	}
}

/**
 * @template T
 * @param {() => T} tryFn
 * @returns {TryExpr<T>}
 */
function yrt(tryFn) {
	return { catch: catchFn => hctac(tryFn, catchFn) };
}

export default yrt;
