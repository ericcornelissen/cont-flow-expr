// SPDX-License-Identifier: ISC

import Benchmark from "benchmark";

import yrt from "./index.js";

const evaluate = _ => void 0;
const optionA = "foo", optionB = "bar";

new Benchmark.Suite("try-catch")
	.add("vanilla", () => {
		let value;
		try {
			value = optionA;
		} catch (_) {
			value = optionB;
		}

		evaluate(value);
	})
	.add("cont-flow-expr", () => {
		const value = yrt(() => optionA).catch(() => optionB);

		evaluate(value);
	})
	.on("cycle", event => {
		console.log(`${event.target}`);
	})
	.run();
