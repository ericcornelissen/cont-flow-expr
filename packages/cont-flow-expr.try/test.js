// SPDX-License-Identifier: ISC

import assert from "node:assert/strict";
import { mock, test } from "node:test";

import fc from "fast-check";

import yrt from "./index.js";

test("try-catch", async (t) => {
	await t.test("no error", () => {
		fc.assert(
			fc.property(
				fc.anything(),
				fc.anything(),
				(tryValue, catchValue) => {
					const tryFn = mock.fn(() => tryValue);
					const catchFn = mock.fn(() => catchValue);
					const scenario = [tryFn, catchFn];

					const expected = nativeTry(...scenario);
					const tryCallCount = tryFn.mock.calls.length;
					const catchCallCount = catchFn.mock.calls.length;
					tryFn.mock.resetCalls();
					catchFn.mock.resetCalls();

					const actual = libraryTry(...scenario);
					assert.equal(actual, expected);
					assert.equal(tryFn.mock.calls.length, tryCallCount);
					assert.equal(catchFn.mock.calls.length, catchCallCount);
				},
			),
		);
	});

	await t.test("an error", () => {
		fc.assert(
			fc.property(
				fc.anything(),
				fc.anything(),
				(error, catchValue) => {
					const tryFn = mock.fn(() => { throw error; });
					const catchFn = mock.fn(() => catchValue);
					const scenario = [tryFn, catchFn];

					const expected = nativeTry(...scenario);
					const tryCallCount = tryFn.mock.calls.length;
					const catchCallCount = catchFn.mock.calls.length;
					tryFn.mock.resetCalls();
					catchFn.mock.resetCalls();

					const actual = libraryTry(...scenario);
					assert.equal(actual, expected);
					assert.equal(tryFn.mock.calls.length, tryCallCount);
					assert.equal(catchFn.mock.calls.length, catchCallCount);
					assert.deepEqual(catchFn.mock.calls[0].arguments, [error]);
				},
			),
		);
	});

	function libraryTry(tryFn, catchFn) {
		return yrt(tryFn).catch(catchFn);
	}

	function nativeTry(tryFn, catchFn) {
		let value;
		try {
			value = tryFn();
		} catch (error) {
			value = catchFn(error);
		}

		return value;
	}
});
