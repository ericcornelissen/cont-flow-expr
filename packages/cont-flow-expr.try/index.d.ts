// SPDX-License-Identifier: ISC

interface TryExpr<T> {
	catch(fn: (error: Error) => T): T;
}

export default function yrt<T>(fn: () => T): TryExpr<T>;
