// SPDX-License-Identifier: ISC

import assert from "node:assert/strict";
import { mock, test } from "node:test";

import fc from "fast-check";

import fi from "./index.js";

const arbCondition = () => fc.oneof(
	fc.anything().filter(value => !!value),
	fc.falsy({ withBigInt: true })
);
const arbValue = () => fc.anything();

test("if-else", async (t) => {
	const arbScenario = () => fc.tuple(
		arbCondition(),
		arbValue(),
		arbValue(),
	);

	await t.test("if-then-else", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				(scenario) => {
					const expected = nativeIf(...scenario);
					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
				},
			),
		);

		function libraryIf(condition, thenValue, elseValue) {
			return fi(condition).then(thenValue).else(elseValue);
		}

		function nativeIf(condition, thenValue, elseValue) {
			let value;
			if (condition) {
				value = thenValue;
			} else {
				value = elseValue;
			}

			return value;
		}
	});

	await t.test("if-thenDo-else", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([ifCondition, thenValue, elseValue]) => {
					const thenFn = mock.fn(() => thenValue);
					const scenario = [ifCondition, thenFn, elseValue];

					const expected = nativeIf(...scenario);
					const thenCallCount = thenFn.mock.calls.length;
					thenFn.mock.resetCalls();

					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
					assert.equal(thenFn.mock.calls.length, thenCallCount);
				},
			),
		);

		function libraryIf(condition, thenFn, elseValue) {
			return fi(condition).thenDo(thenFn).else(elseValue);
		}

		function nativeIf(condition, thenFn, elseValue) {
			let value;
			if (condition) {
				value = thenFn();
			} else {
				value = elseValue;
			}

			return value;
		}
	});

	await t.test("if-then-elseDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([ifCondition, thenValue, elseValue]) => {
					const elseFn = mock.fn(() => elseValue);
					const scenario = [ifCondition, thenValue, elseFn];

					const expected = nativeIf(...scenario);
					const elseCallCount = elseFn.mock.calls.length;
					elseFn.mock.resetCalls();

					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
					assert.equal(elseFn.mock.calls.length, elseCallCount);
				},
			),
		);

		function libraryIf(condition, thenValue, elseFn) {
			return fi(condition).then(thenValue).elseDo(elseFn);
		}

		function nativeIf(condition, thenValue, elseFn) {
			let value;
			if (condition) {
				value = thenValue;
			} else {
				value = elseFn();
			}

			return value;
		}
	});

	await t.test("if-thenDo-elseDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([ifCondition, thenValue, elseValue]) => {
					const thenFn = mock.fn(() => thenValue);
					const elseFn = mock.fn(() => elseValue);
					const scenario = [ifCondition, thenFn, elseFn];

					const expected = nativeIf(...scenario);
					const thenCallCount = thenFn.mock.calls.length;
					const elseCallCount = elseFn.mock.calls.length;
					thenFn.mock.resetCalls();
					elseFn.mock.resetCalls();

					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
					assert.equal(thenFn.mock.calls.length, thenCallCount);
					assert.equal(elseFn.mock.calls.length, elseCallCount);
				},
			),
		);

		function libraryIf(condition, thenFn, elseFn) {
			return fi(condition).thenDo(thenFn).elseDo(elseFn);
		}

		function nativeIf(condition, thenFn, elseFn) {
			let value;
			if (condition) {
				value = thenFn();
			} else {
				value = elseFn();
			}

			return value;
		}
	});
});

test("if-elseIf-else", async (t) => {
	const arbScenario = () => fc.tuple(
		arbCondition(),
		arbValue(),
		arbCondition(),
		arbValue(),
		arbValue(),
	);

	await t.test("if-then-elseIf-then-else", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				(scenario) => {
					const expected = nativeIf(...scenario);
					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
				},
			),
		);

		function libraryIf(ifCondition, thenValue, elseIfCondition, elseThenValue, elseValue) {
			return fi(ifCondition).then(thenValue)
				.elseIf(elseIfCondition).then(elseThenValue)
				.else(elseValue);
		}

		function nativeIf(ifCondition, thenValue, elseIfCondition, elseThenValue, elseValue) {
			let value;
			if (ifCondition) {
				value = thenValue;
			} else if (elseIfCondition) {
				value = elseThenValue;
			} else {
				value = elseValue;
			}

			return value;
		}
	});

	await t.test("if-thenDo-elseIf-then-else", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([ifCondition, thenValue, elseIfCondition, elseThenValue, elseValue]) => {
					const thenFn = mock.fn(() => thenValue);
					const scenario = [ifCondition, thenFn, elseIfCondition, elseThenValue, elseValue];

					const expected = nativeIf(...scenario);
					const thenCallCount = thenFn.mock.calls.length;
					thenFn.mock.resetCalls();

					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
					assert.equal(thenFn.mock.calls.length, thenCallCount);
				},
			),
		);

		function libraryIf(ifCondition, thenFn, elseIfCondition, elseThenValue, elseValue) {
			return fi(ifCondition).thenDo(thenFn)
				.elseIf(elseIfCondition).then(elseThenValue)
				.else(elseValue);
		}

		function nativeIf(ifCondition, thenFn, elseIfCondition, elseThenValue, elseValue) {
			let value;
			if (ifCondition) {
				value = thenFn();
			} else if (elseIfCondition) {
				value = elseThenValue;
			} else {
				value = elseValue;
			}

			return value;
		}
	});

	await t.test("if-then-elseIf-thenDo-else", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([ifCondition, thenValue, elseIfCondition, elseThenValue, elseValue]) => {
					const elseThenFn = mock.fn(() => elseThenValue);
					const scenario = [ifCondition, thenValue, elseIfCondition, elseThenFn, elseValue];

					const expected = nativeIf(...scenario);
					const elseThenCallCount = elseThenFn.mock.calls.length;
					elseThenFn.mock.resetCalls();

					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
					assert.equal(elseThenFn.mock.calls.length, elseThenCallCount);
				},
			),
		);

		function libraryIf(ifCondition, thenValue, elseIfCondition, elseThenFn, elseValue) {
			return fi(ifCondition).then(thenValue)
				.elseIf(elseIfCondition).thenDo(elseThenFn)
				.else(elseValue);
		}

		function nativeIf(ifCondition, thenValue, elseIfCondition, elseThenFn, elseValue) {
			let value;
			if (ifCondition) {
				value = thenValue;
			} else if (elseIfCondition) {
				value = elseThenFn();
			} else {
				value = elseValue;
			}

			return value;
		}
	});

	await t.test("if-then-elseIf-then-elseDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([ifCondition, thenValue, elseIfCondition, elseThenValue, elseValue]) => {
					const elseFn = mock.fn(() => elseValue);
					const scenario = [ifCondition, thenValue, elseIfCondition, elseThenValue, elseFn];

					const expected = nativeIf(...scenario);
					const elseCallCount = elseFn.mock.calls.length;
					elseFn.mock.resetCalls();

					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
					assert.equal(elseFn.mock.calls.length, elseCallCount);
				},
			),
		);

		function libraryIf(ifCondition, thenValue, elseIfCondition, elseThenValue, elseFn) {
			return fi(ifCondition).then(thenValue)
				.elseIf(elseIfCondition).then(elseThenValue)
				.elseDo(elseFn);
		}

		function nativeIf(ifCondition, thenValue, elseIfCondition, elseThenValue, elseFn) {
			let value;
			if (ifCondition) {
				value = thenValue;
			} else if (elseIfCondition) {
				value = elseThenValue;
			} else {
				value = elseFn();
			}

			return value;
		}
	});

	await t.test("if-thenDo-elseIf-thenDo-else", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([ifCondition, thenValue, elseIfCondition, elseThenValue, elseValue]) => {
					const thenFn = mock.fn(() => thenValue);
					const elseThenFn = mock.fn(() => elseThenValue);
					const scenario = [ifCondition, thenFn, elseIfCondition, elseThenFn, elseValue];

					const expected = nativeIf(...scenario);
					const thenCallCount = thenFn.mock.calls.length;
					const elseThenCallCount = elseThenFn.mock.calls.length;
					thenFn.mock.resetCalls();
					elseThenFn.mock.resetCalls();

					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
					assert.equal(thenFn.mock.calls.length, thenCallCount);
					assert.equal(elseThenFn.mock.calls.length, elseThenCallCount);
				},
			),
		);

		function libraryIf(ifCondition, thenFn, elseIfCondition, elseThenFn, elseValue) {
			return fi(ifCondition).thenDo(thenFn)
				.elseIf(elseIfCondition).thenDo(elseThenFn)
				.else(elseValue);
		}

		function nativeIf(ifCondition, thenFn, elseIfCondition, elseThenFn, elseValue) {
			let value;
			if (ifCondition) {
				value = thenFn();
			} else if (elseIfCondition) {
				value = elseThenFn();
			} else {
				value = elseValue;
			}

			return value;
		}
	});

	await t.test("if-thenDo-elseIf-then-elseDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([ifCondition, thenValue, elseIfCondition, elseThenValue, elseValue]) => {
					const thenFn = mock.fn(() => thenValue);
					const elseFn = mock.fn(() => elseValue);
					const scenario = [ifCondition, thenFn, elseIfCondition, elseThenValue, elseFn];

					const expected = nativeIf(...scenario);
					const thenCallCount = thenFn.mock.calls.length;
					const elseCallCount = elseFn.mock.calls.length;
					thenFn.mock.resetCalls();
					elseFn.mock.resetCalls();

					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
					assert.equal(thenFn.mock.calls.length, thenCallCount);
					assert.equal(elseFn.mock.calls.length, elseCallCount);
				},
			),
		);

		function libraryIf(ifCondition, thenFn, elseIfCondition, elseThenValue, elseFn) {
			return fi(ifCondition).thenDo(thenFn)
				.elseIf(elseIfCondition).then(elseThenValue)
				.elseDo(elseFn);
		}

		function nativeIf(ifCondition, thenFn, elseIfCondition, elseThenValue, elseFn) {
			let value;
			if (ifCondition) {
				value = thenFn();
			} else if (elseIfCondition) {
				value = elseThenValue;
			} else {
				value = elseFn();
			}

			return value;
		}
	});

	await t.test("if-then-elseIf-thenDo-elseDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([ifCondition, thenValue, elseIfCondition, elseThenValue, elseValue]) => {
					const elseThenFn = mock.fn(() => elseThenValue);
					const elseFn = mock.fn(() => elseValue);
					const scenario = [ifCondition, thenValue, elseIfCondition, elseThenFn, elseFn];

					const expected = nativeIf(...scenario);
					const elseThenCallCount = elseThenFn.mock.calls.length;
					const elseCallCount = elseFn.mock.calls.length;
					elseThenFn.mock.resetCalls();
					elseFn.mock.resetCalls();

					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
					assert.equal(elseThenFn.mock.calls.length, elseThenCallCount);
					assert.equal(elseFn.mock.calls.length, elseCallCount);
				},
			),
		);

		function libraryIf(ifCondition, thenValue, elseIfCondition, elseThenFn, elseFn) {
			return fi(ifCondition).then(thenValue)
				.elseIf(elseIfCondition).thenDo(elseThenFn)
				.elseDo(elseFn);
		}

		function nativeIf(ifCondition, thenValue, elseIfCondition, elseThenFn, elseFn) {
			let value;
			if (ifCondition) {
				value = thenValue;
			} else if (elseIfCondition) {
				value = elseThenFn();
			} else {
				value = elseFn();
			}

			return value;
		}
	});

	await t.test("if-thenDo-elseIf-thenDo-elseDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([ifCondition, thenValue, elseIfCondition, elseThenValue, elseValue]) => {
					const thenFn = mock.fn(() => thenValue);
					const elseThenFn = mock.fn(() => elseThenValue);
					const elseFn = mock.fn(() => elseValue);
					const scenario = [ifCondition, thenFn, elseIfCondition, elseThenFn, elseFn];

					const expected = nativeIf(...scenario);
					const thenCallCount = thenFn.mock.calls.length;
					const elseThenCallCount = elseThenFn.mock.calls.length;
					const elseCallCount = elseFn.mock.calls.length;
					thenFn.mock.resetCalls();
					elseThenFn.mock.resetCalls();
					elseFn.mock.resetCalls();

					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
					assert.equal(thenFn.mock.calls.length, thenCallCount);
					assert.equal(elseThenFn.mock.calls.length, elseThenCallCount);
					assert.equal(elseFn.mock.calls.length, elseCallCount);
				},
			),
		);

		function libraryIf(ifCondition, thenFn, elseIfCondition, elseThenFn, elseFn) {
			return fi(ifCondition).thenDo(thenFn)
				.elseIf(elseIfCondition).thenDo(elseThenFn)
				.elseDo(elseFn);
		}

		function nativeIf(ifCondition, thenFn, elseIfCondition, elseThenFn, elseFn) {
			let value;
			if (ifCondition) {
				value = thenFn();
			} else if (elseIfCondition) {
				value = elseThenFn();
			} else {
				value = elseFn();
			}

			return value;
		}
	});

	await t.test("if-then-elseifLazy-then-else", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([ifCondition, thenValue, elseIfCondition, elseThenValue, elseValue]) => {
					const elseIfFn = mock.fn(() => elseIfCondition);
					const scenario = [ifCondition, thenValue, elseIfFn, elseThenValue, elseValue];

					const expected = nativeIf(...scenario);
					const elseIfCallCount = elseIfFn.mock.calls.length;
					elseIfFn.mock.resetCalls();

					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
					assert.equal(elseIfFn.mock.calls.length, elseIfCallCount);
				},
			),
		);

		function libraryIf(ifCondition, thenValue, elseIfFn, elseThenValue, elseValue) {
			return fi(ifCondition).then(thenValue)
				.elseIfLazy(elseIfFn).then(elseThenValue)
				.else(elseValue);
		}

		function nativeIf(ifCondition, ifValue, elseIfFn, elseThenValue, elseValue) {
			let value;
			if (ifCondition) {
				value = ifValue;
			} else if (elseIfFn()) {
				value = elseThenValue;
			} else {
				value = elseValue;
			}

			return value;
		}
	});

	await t.test("if-thenDo-elseifLazy-then-else", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([ifCondition, thenValue, elseIfCondition, elseThenValue, elseValue]) => {
					const thenFn = mock.fn(() => thenValue);
					const elseIfFn = mock.fn(() => elseIfCondition);
					const scenario = [ifCondition, thenFn, elseIfFn, elseThenValue, elseValue];

					const expected = nativeIf(...scenario);
					const thenCallCount = thenFn.mock.calls.length;
					const elseIfCallCount = elseIfFn.mock.calls.length;
					thenFn.mock.resetCalls();
					elseIfFn.mock.resetCalls();

					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
					assert.equal(thenFn.mock.calls.length, thenCallCount);
					assert.equal(elseIfFn.mock.calls.length, elseIfCallCount);
				},
			),
		);

		function libraryIf(ifCondition, thenFn, elseIfFn, elseThenValue, elseValue) {
			return fi(ifCondition).thenDo(thenFn)
				.elseIfLazy(elseIfFn).then(elseThenValue)
				.else(elseValue);
		}

		function nativeIf(ifCondition, thenFn, elseIfFn, elseThenValue, elseValue) {
			let value;
			if (ifCondition) {
				value = thenFn();
			} else if (elseIfFn()) {
				value = elseThenValue;
			} else {
				value = elseValue;
			}

			return value;
		}
	});

	await t.test("if-then-elseifLazy-thenDo-else", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([ifCondition, thenValue, elseIfCondition, elseThenValue, elseValue]) => {
					const elseIfFn = mock.fn(() => elseIfCondition);
					const elseThenFn = mock.fn(() => elseThenValue);
					const scenario = [ifCondition, thenValue, elseIfFn, elseThenFn, elseValue];

					const expected = nativeIf(...scenario);
					const elseIfCallCount = elseIfFn.mock.calls.length;
					const elseThenCallCount = elseThenFn.mock.calls.length;
					elseIfFn.mock.resetCalls();
					elseThenFn.mock.resetCalls();

					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
					assert.equal(elseIfFn.mock.calls.length, elseIfCallCount);
					assert.equal(elseThenFn.mock.calls.length, elseThenCallCount);
				},
			),
		);

		function libraryIf(ifCondition, thenValue, elseIfFn, elseThenFn, elseValue) {
			return fi(ifCondition).then(thenValue)
				.elseIfLazy(elseIfFn).thenDo(elseThenFn)
				.else(elseValue);
		}

		function nativeIf(ifCondition, thenValue, elseIfFn, elseThenFn, elseValue) {
			let value;
			if (ifCondition) {
				value = thenValue;
			} else if (elseIfFn()) {
				value = elseThenFn();
			} else {
				value = elseValue;
			}

			return value;
		}
	});

	await t.test("if-then-elseifLazy-then-elseDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([ifCondition, thenValue, elseIfCondition, elseThenValue, elseValue]) => {
					const elseIfFn = mock.fn(() => elseIfCondition);
					const elseFn = mock.fn(() => elseValue);
					const scenario = [ifCondition, thenValue, elseIfFn, elseThenValue, elseFn];

					const expected = nativeIf(...scenario);
					const elseIfCallCount = elseIfFn.mock.calls.length;
					const elseCallCount = elseFn.mock.calls.length;
					elseIfFn.mock.resetCalls();
					elseFn.mock.resetCalls();

					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
					assert.equal(elseIfFn.mock.calls.length, elseIfCallCount);
					assert.equal(elseFn.mock.calls.length, elseCallCount);
				},
			),
		);

		function libraryIf(ifCondition, thenValue, elseIfFn, elseThenValue, elseFn) {
			return fi(ifCondition).then(thenValue)
				.elseIfLazy(elseIfFn).then(elseThenValue)
				.elseDo(elseFn);
		}

		function nativeIf(ifCondition, thenValue, elseIfFn, elseThenValue, elseFn) {
			let value;
			if (ifCondition) {
				value = thenValue;
			} else if (elseIfFn()) {
				value = elseThenValue;
			} else {
				value = elseFn();
			}

			return value;
		}
	});

	await t.test("if-thenDo-elseifLazy-thenDo-else", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([ifCondition, thenValue, elseIfCondition, elseThenValue, elseValue]) => {
					const thenFn = mock.fn(() => thenValue);
					const elseIfFn = mock.fn(() => elseIfCondition);
					const elseThenFn = mock.fn(() => elseThenValue);
					const scenario = [ifCondition, thenFn, elseIfFn, elseThenFn, elseValue];

					const expected = nativeIf(...scenario);
					const thenCallCount = thenFn.mock.calls.length;
					const elseIfCallCount = elseIfFn.mock.calls.length;
					const elseThenCallCount = elseThenFn.mock.calls.length;
					thenFn.mock.resetCalls();
					elseIfFn.mock.resetCalls();
					elseThenFn.mock.resetCalls();

					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
					assert.equal(thenFn.mock.calls.length, thenCallCount);
					assert.equal(elseIfFn.mock.calls.length, elseIfCallCount);
					assert.equal(elseThenFn.mock.calls.length, elseThenCallCount);
				},
			),
		);

		function libraryIf(ifCondition, thenFn, elseIfFn, elseThenFn, elseValue) {
			return fi(ifCondition).thenDo(thenFn)
				.elseIfLazy(elseIfFn).thenDo(elseThenFn)
				.else(elseValue);
		}

		function nativeIf(ifCondition, thenFn, elseIfFn, elseThenFn, elseValue) {
			let value;
			if (ifCondition) {
				value = thenFn();
			} else if (elseIfFn()) {
				value = elseThenFn();
			} else {
				value = elseValue;
			}

			return value;
		}
	});

	await t.test("if-thenDo-elseifLazy-then-elseDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([ifCondition, thenValue, elseIfCondition, elseThenValue, elseValue]) => {
					const thenFn = mock.fn(() => thenValue);
					const elseIfFn = mock.fn(() => elseIfCondition);
					const elseFn = mock.fn(() => elseValue);
					const scenario = [ifCondition, thenFn, elseIfFn, elseThenValue, elseFn];

					const expected = nativeIf(...scenario);
					const thenCallCount = thenFn.mock.calls.length;
					const elseIfCallCount = elseIfFn.mock.calls.length;
					const elseCallCount = elseFn.mock.calls.length;
					thenFn.mock.resetCalls();
					elseIfFn.mock.resetCalls();
					elseFn.mock.resetCalls();

					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
					assert.equal(thenFn.mock.calls.length, thenCallCount);
					assert.equal(elseIfFn.mock.calls.length, elseIfCallCount);
					assert.equal(elseFn.mock.calls.length, elseCallCount);
				},
			),
		);

		function libraryIf(ifCondition, thenFn, elseIfFn, elseThenValue, elseFn) {
			return fi(ifCondition).thenDo(thenFn)
				.elseIfLazy(elseIfFn).then(elseThenValue)
				.elseDo(elseFn);
		}

		function nativeIf(ifCondition, thenFn, elseIfFn, elseThenValue, elseFn) {
			let value;
			if (ifCondition) {
				value = thenFn();
			} else if (elseIfFn()) {
				value = elseThenValue;
			} else {
				value = elseFn();
			}

			return value;
		}
	});

	await t.test("if-then-elseifLazy-thenDo-elseDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([ifCondition, thenValue, elseIfCondition, elseThenValue, elseValue]) => {
					const elseIfFn = mock.fn(() => elseIfCondition);
					const elseThenFn = mock.fn(() => elseThenValue);
					const elseFn = mock.fn(() => elseValue);
					const scenario = [ifCondition, thenValue, elseIfFn, elseThenFn, elseFn];

					const expected = nativeIf(...scenario);
					const elseIfCallCount = elseIfFn.mock.calls.length;
					const elseThenCallCount = elseThenFn.mock.calls.length;
					const elseCallCount = elseFn.mock.calls.length;
					elseIfFn.mock.resetCalls();
					elseThenFn.mock.resetCalls();
					elseFn.mock.resetCalls();

					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
					assert.equal(elseIfFn.mock.calls.length, elseIfCallCount);
					assert.equal(elseThenFn.mock.calls.length, elseThenCallCount);
					assert.equal(elseFn.mock.calls.length, elseCallCount);
				},
			),
		);

		function libraryIf(ifCondition, thenValue, elseIfFn, elseThenFn, elseFn) {
			return fi(ifCondition).then(thenValue)
				.elseIfLazy(elseIfFn).thenDo(elseThenFn)
				.elseDo(elseFn);
		}

		function nativeIf(ifCondition, thenValue, elseIfFn, elseThenFn, elseFn) {
			let value;
			if (ifCondition) {
				value = thenValue;
			} else if (elseIfFn()) {
				value = elseThenFn();
			} else {
				value = elseFn();
			}

			return value;
		}
	});

	await t.test("if-thenDo-elseifLazy-thenDo-elseDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([ifCondition, thenValue, elseIfCondition, elseThenValue, elseValue]) => {
					const thenFn = mock.fn(() => thenValue);
					const elseIfFn = mock.fn(() => elseIfCondition);
					const elseThenFn = mock.fn(() => elseThenValue);
					const elseFn = mock.fn(() => elseValue);
					const scenario = [ifCondition, thenFn, elseIfFn, elseThenFn, elseFn];

					const expected = nativeIf(...scenario);
					const thenCallCount = thenFn.mock.calls.length;
					const elseIfCallCount = elseIfFn.mock.calls.length;
					const elseThenCallCount = elseThenFn.mock.calls.length;
					const elseCallCount = elseFn.mock.calls.length;
					thenFn.mock.resetCalls();
					elseIfFn.mock.resetCalls();
					elseThenFn.mock.resetCalls();
					elseFn.mock.resetCalls();

					const actual = libraryIf(...scenario);
					assert.equal(actual, expected);
					assert.equal(thenFn.mock.calls.length, thenCallCount);
					assert.equal(elseIfFn.mock.calls.length, elseIfCallCount);
					assert.equal(elseThenFn.mock.calls.length, elseThenCallCount);
					assert.equal(elseFn.mock.calls.length, elseCallCount);
				},
			),
		);

		function libraryIf(ifCondition, thenFn, elseIfFn, elseThenFn, elseFn) {
			return fi(ifCondition).thenDo(thenFn)
				.elseIfLazy(elseIfFn).thenDo(elseThenFn)
				.elseDo(elseFn);
		}

		function nativeIf(ifCondition, thenFn, elseIfFn, elseThenFn, elseFn) {
			let value;
			if (ifCondition) {
				value = thenFn();
			} else if (elseIfFn()) {
				value = elseThenFn();
			} else {
				value = elseFn();
			}

			return value;
		}
	});
});

test("arbitrary if expression", () => {
	const NO_CONDITION = void 0, NO_RESULT = void 0;

	class EagerCondition {
		constructor(condition) {
			this.condition = condition;
		}

		check(model) {
			return model.lastCondition === NO_CONDITION;
		}

		run(model, real) {
			model.lastCondition = this.condition;
			real.expr = real.expr.elseIf(this.condition);
		}

		toString() {
			return `elseIf(${this.condition})`;
		}
	}

	class LazyCondition {
		constructor(condition) {
			this.condition = condition;
		}

		check(model) {
			return model.lastCondition === NO_CONDITION;
		}

		run(model, real) {
			model.lastCondition = this.condition;

			const fn = mock.fn(() => this.condition);
			real.expr = real.expr.elseIfLazy(fn);

			assert.equal(fn.mock.calls.length, model.result === NO_RESULT ? 1 : 0);
		}

		toString() {
			return `elseIfLazy(() => ${this.condition})`;
		}
	}

	class EagerBranch {
		constructor(value) {
			this.value = value;
		}

		check(model) {
			return model.lastCondition !== NO_CONDITION;
		}

		run(model, real) {
			const branchTaken = model.result === NO_RESULT && !!model.lastCondition;

			model.lastCondition = NO_CONDITION;
			if (branchTaken) {
				model.result = this.value;
			}

			real.expr = real.expr.then(this.value);
		}

		toString() {
			return `then(${this.value})`;
		}
	}

	class LazyBranch {
		constructor(value) {
			this.value = value;
		}

		check(model) {
			return model.lastCondition !== NO_CONDITION;
		}

		run(model, real) {
			const branchTaken = model.result === NO_RESULT && !!model.lastCondition;

			model.lastCondition = NO_CONDITION;
			if (branchTaken) {
				model.result = this.value;
			}

			const fn = mock.fn(() => this.value);
			real.expr = real.expr.thenDo(fn);

			assert.equal(fn.mock.calls.length, branchTaken ? 1 : 0);
		}

		toString() {
			return `thenDo(() => ${this.value})`;
		}
	}

	class EagerElse {
		constructor(value) {
			this.value = value;
		}

		check(model) {
			return model.lastCondition === NO_CONDITION;
		}

		run(model, real) {
			const branchTaken = model.result === NO_RESULT;
			const expected = branchTaken ? this.value : model.result;

			const actual = real.expr.else(this.value);

			assert.equal(actual, expected);
		}

		toString() {
			return `else(${this.value})`;
		}
	}

	class LazyElse {
		constructor(value) {
			this.value = value;
		}

		check(model) {
			return model.lastCondition === NO_CONDITION;
		}

		run(model, real) {
			const branchTaken = model.result === NO_RESULT;
			const expected = branchTaken ? this.value : model.result;

			const fn = mock.fn(() => this.value);
			const actual = real.expr.elseDo(fn);

			assert.equal(actual, expected);
			assert.equal(fn.mock.calls.length, branchTaken ? 1 : 0);
		}

		toString() {
			return `elseDo(() => ${this.value})`;
		}
	}

	const commands = [
		arbCondition()
			.filter(condition => condition !== NO_CONDITION)
			.map(condition => new EagerCondition(condition)),
		arbCondition()
			.filter(condition => condition !== NO_CONDITION)
			.map(condition => new LazyCondition(condition)),
		arbValue()
			.filter(value => value !== NO_RESULT)
			.map(value => new EagerBranch(value)),
		arbValue()
			.filter(value => value !== NO_RESULT)
			.map(value => new LazyBranch(value)),
		arbValue()
			.filter(value => value !== NO_RESULT)
			.map(value => new EagerElse(value)),
		arbValue()
			.filter(value => value !== NO_RESULT)
			.map(value => new LazyElse(value)),
	];

	fc.assert(
		fc.property(
			fc.boolean(),
			fc.commands(commands, { size: '+1' }),
			(initialCondition, commands) => {
				const setup = () => ({
					model: { lastCondition: initialCondition, result: NO_RESULT },
					real: { expr: fi(initialCondition) },
				});

				fc.modelRun(setup, commands);
			},
		),
	);
});
