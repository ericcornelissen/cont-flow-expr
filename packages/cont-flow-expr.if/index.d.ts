// SPDX-License-Identifier: ISC

interface IfExpr<T> {
	then(value: T): ThenExpr<T>;
	thenDo(fn: () => T): ThenExpr<T>;
}

interface ThenExpr<T> {
	else(value: T): T;
	elseDo(fn: () => T): T;
	elseIf(condition: boolean): IfExpr<T>;
	elseIfLazy(fn: () => boolean): IfExpr<T>;
}

export default function fi<T>(condition: boolean): IfExpr<T>;
