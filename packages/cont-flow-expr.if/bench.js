// SPDX-License-Identifier: ISC

import Benchmark from "benchmark";

import { $if } from "dolla-if";
import fi from "./index.js";

const evaluate = _ => void 0;
const condition = true, optionA = "foo", optionB = "bar";

new Benchmark.Suite("if-else")
	.add("vanilla", () => {
		let value;
		if (condition) {
			value = optionA;
		} else {
			value = optionB;
		}

		evaluate(value);
	})
	.add("cont-flow-expr", () => {
		const value = fi(condition)
			.then(optionA)
			.else(optionB);

		evaluate(value);
	})
	.add("dolla-if", () => {
		const value = $if(condition)
			.then(optionA)
			.else(optionB);

		evaluate(value);
	})
	.on("cycle", event => {
		console.log(`${event.target}`);
	})
	.run();
