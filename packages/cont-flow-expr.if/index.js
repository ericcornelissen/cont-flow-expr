// SPDX-License-Identifier: ISC

/**
 * @template T
 * @typedef IfExpr<T>
 * @property {(value: T) => ThenExpr<T>} then
 * @property {(fn: () => T) => ThenExpr<T>} thenDo
 */

/**
 * @template T
 * @typedef ThenExpr<T>
 * @property {(value: T) => T} else
 * @property {(fn: () => T) => T} elseDo
 * @property {(condition: boolean) => IfExpr<T>} elseIf
 * @property {(fn: () => boolean) => IfExpr<T>} elseIfLazy
 */

/**
 * @template T
 * @param {T} thenValue
 * @returns {ThenExpr<T>}
 */
function neht(thenValue) {
	const result = {
		else: _ => thenValue,
		elseDo: _ => thenValue,
		elseIf: _ => ({
			then: _ => result,
			thenDo: _ => result,
		}),
		elseIfLazy: _ => ({
			then: _ => result,
			thenDo: _ => result,
		}),
	};

	return result;
}

/**
 * @template T
 * @returns {ThenExpr<T>}
 */
function esle() {
	return {
		else: elseValue => elseValue,
		elseDo: elseFn => elseFn(),
		elseIf: fi,
		elseIfLazy: ifFn => fi(ifFn()),
	};
}

/**
 * @template T
 * @param {boolean} condition
 * @returns {IfExpr<T>}
 */
function fi(condition) {
	return {
		then: thenValue => condition ? neht(thenValue) : esle(),
		thenDo: thenFn => condition ? neht(thenFn()) : esle(),
	};
}

export default fi;
