# `cont-flow-expr`

What if you could write something like:

```javascript
const value = if (condition) {
  optionA;
} else {
  optionB;
}
```

Enter, `cont-flow-expr`:

```javascript
import { fi } from "cont-flow-expr";

const value = fi(condition)
  .then(optionA)
  .else(optionB);
```

## API

### `if-elseif*-else`

A basic `if-else` expression can be written as:

```javascript
import { fi } from "cont-flow-expr";
// or: import fi from "cont-flow-expr/if";

const value = fi(condition)
  .then(optionA)
  .else(optionB);
```

An `if-elseif-else` expression can be written as:

```javascript
import { fi } from "cont-flow-expr";

const value =
  fi(condition1).then(optionA)
  .elseIf(condition2).then(optionB)
  // .elseIf(conditionN-1).then(optionN-1)
  .else(optionZ);
```

with arbitrarily many uses of `.elseIf().then()`.

Branches can also be evaluated lazily, which can mixed with normal branches arbitrarily:

```javascript
import { fi } from "cont-flow-expr";

const value =
  fi(condition1).then(optionA)
  .elseIf(condition2).thenDo(() => optionB)
  .else(optionC);
```

See [`cont-flow-expr.if`] for more details.

[`cont-flow-expr.if`]: https://www.npmjs.com/package/cont-flow-expr.if

### `switch-case*-default`

A `switch-case` expression can be written as:

```javascript
import { hctiws } from "cont-flow-expr";
// or: import hctiws from "cont-flow-expr/switch";

const value = hctiws(query)
  .case(candidate1).then(optionA)
  .case(candidate2).then(optionB)
  // .case(candidateN-1).then(optionN-1)
  .default(optionZ);
```

with arbitrarily many uses of `.case().then()`.

Branches can also be evaluated lazily, which can mixed with normal branches arbitrarily:

```javascript
import { hctiws } from "cont-flow-expr";

const value = hctiws(query)
  .case(candidate1).thenDo(() => optionA)
  .case(candidate2).then(optionB)
  .defaultDo(() => optionC);
```

See [`cont-flow-expr.switch`] for more details.

[`cont-flow-expr.switch`]: https://www.npmjs.com/package/cont-flow-expr.switch

### `try-catch`

A `try-catch` expression can be written as:

```javascript
import { yrt } from "cont-flow-expr";
// or: import yrt from "cont-flow-expr/try";

const value = yrt(() => {
  // Potentially failing commands
  return optionA;
}).catch(() => {
  // Recovering commands
  return optionB;
});
```

See [`cont-flow-expr.try`] for more details.

[`cont-flow-expr.try`]: https://www.npmjs.com/package/cont-flow-expr.try

## TypeScript

[TypeScript] support is included and can be used to ensure all branches of a given expression
evaluate to the same type (union). For this to work you have to provide a type annotation. For
example:

```typescript
import { fi } from "cont-flow-expr";

const condition = true, optionA = "foo", optionB = "bar";

const aString = fi<string>(condition)
  .then(optionA)
  .else(optionB);
```

[TypeScript]: https://www.typescriptlang.org/

## Benchmarks

The benchmarks in this project - which are powered by [Benchmark.js] - can be used to compare this
library against vanilla JavaScript as well as alternative libraries. Here's a sample:

```sh
if-else
vanilla x 1,409,198,725 ops/sec ±0.16% (97 runs sampled)
cont-flow-expr x 68,040,893 ops/sec ±0.53% (96 runs sampled)
dolla-if x 60,359,107 ops/sec ±0.36% (99 runs sampled)

switch-case
vanilla x 1,411,831,771 ops/sec ±0.15% (98 runs sampled)
cont-flow-expr x 15,601,299 ops/sec ±0.83% (94 runs sampled)

try-catch
vanilla x 1,401,337,499 ops/sec ±0.15% (91 runs sampled)
cont-flow-expr x 70,774,351 ops/sec ±0.20% (96 runs sampled)
```

[benchmark.js]: https://benchmarkjs.com/

## License

The source code is licensed under the ISC license. The documentation text is licensed under
[CC BY 4.0].

[cc by 4.0]: https://creativecommons.org/licenses/by/4.0/
