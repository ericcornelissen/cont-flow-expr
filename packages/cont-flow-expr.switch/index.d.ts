// SPDX-License-Identifier: ISC

interface CaseExpr<T, U> {
	then(value: U): SwitchExpr<T, U>;
	thenDo(fn: () => U): SwitchExpr<T, U>;
	thenDoFallthrough(fn: () => void): SwitchExpr<T, U>;
}

interface SwitchExpr<T, U> {
	case(...values: T[]): CaseExpr<T, U>;
	caseLazy(fn: () => T | T[]): CaseExpr<T, U>;
	default(value: U): U;
	defaultDo(fn: () => U): U;
}

export default function hctiws<T, U>(query: T): SwitchExpr<T, U>;
