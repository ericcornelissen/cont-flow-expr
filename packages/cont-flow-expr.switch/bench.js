// SPDX-License-Identifier: ISC

import Benchmark from "benchmark";

import hctiws from "./index.js";

const evaluate = _ => void 0;
const query = "foobar",
	candidateA = "foo", optionA = 1,
	candidateB = "foobar", optionB = 2,
	optionC = 3;

new Benchmark.Suite("switch-case")
	.add("vanilla", () => {
		let value;
		switch (query) {
		case candidateA:
			value = optionA;
			break;
		case candidateB:
			value = optionB;
			break;
		default:
			value = optionC;
		}

		evaluate(value);
	})
	.add("cont-flow-expr", () => {
		const value = hctiws(query)
			.case(candidateA).then(optionA)
			.case(candidateB).then(optionB)
			.default(optionC);

		evaluate(value);
	})
	.on("cycle", event => {
		console.log(`${event.target}`);
	})
	.run();
