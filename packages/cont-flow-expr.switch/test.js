// SPDX-License-Identifier: ISC

import assert from "node:assert/strict";
import { mock, test } from "node:test";

import fc from "fast-check";

import hctiws from "./index.js";

const arbQuery = () => fc.oneof(
	fc.bigInt(),
	fc.boolean(),
	fc.char(),
	fc.date(),
	fc.double({ noNaN: true }),
	fc.float({ noNaN: true }),
	fc.integer(),
	fc.string(),
);
const arbValue = () => fc.anything();

test("switch-default", async (t) => {
	const arbScenario = () => fc.tuple(
		arbQuery(),
		arbValue(),
	);

	await t.test("switch-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				(scenario) => {
					const expected = nativeSwitch(...scenario);
					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
				},
			),
		);

		function librarySwitch(query, defaultValue) {
			return hctiws(query).default(defaultValue);
		}

		function nativeSwitch(query, defaultValue) {
			let value;
			switch (query) {
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, defaultValue]) => {
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, defaultFn];

					const expected = nativeSwitch(...scenario);
					const defaultCallCount = defaultFn.mock.calls.length;
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, defaultFn) {
			return hctiws(query).defaultDo(defaultFn);
		}

		function nativeSwitch(query, defaultFn) {
			let value;
			switch (query) {
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});
});

test("switch-case-default", async (t) => {
	const arbScenario = () => fc.oneof(
		fc.tuple(arbQuery(), arbQuery(), arbValue(), arbValue()),
		fc.tuple(arbQuery(), arbValue(), arbValue())
			.map(([query, caseValue, defaultValue]) => [query, query, caseValue, defaultValue]),
	);

	await t.test("switch-case-then-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				(scenario) => {
					const expected = nativeSwitch(...scenario);
					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
				},
			),
		);

		function librarySwitch(query, candidate, caseValue, defaultValue) {
			return hctiws(query)
				.case(candidate).then(caseValue)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate, caseValue, defaultValue) {
			let value;
			switch (query) {
			case candidate:
				value = caseValue;
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDo-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate, caseValue, defaultValue]) => {
					const caseFn = mock.fn(() => caseValue);
					const scenario = [query, candidate, caseFn, defaultValue];

					const expected = nativeSwitch(...scenario);
					const caseCallCount = caseFn.mock.calls.length;
					caseFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn.mock.calls.length, caseCallCount);
				},
			),
		);

		function librarySwitch(query, candidate, caseFn, defaultValue) {
			return hctiws(query)
				.case(candidate).thenDo(caseFn)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate, caseFn, defaultValue) {
			let value;
			switch (query) {
			case candidate:
				value = caseFn();
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDoFallthrough-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate, caseValue, defaultValue]) => {
					const caseFn = mock.fn(() => caseValue);
					const scenario = [query, candidate, caseFn, defaultValue];

					const expected = nativeSwitch(...scenario);
					const caseCallCount = caseFn.mock.calls.length;
					caseFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn.mock.calls.length, caseCallCount);
				},
			),
		);

		function librarySwitch(query, candidate, caseFn, defaultValue) {
			return hctiws(query)
				.case(candidate).thenDoFallthrough(caseFn)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate, caseFn, defaultValue) {
			let value;
			switch (query) {
			case candidate:
				value = caseFn();
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-then-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate, caseValue, defaultValue]) => {
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate, caseValue, defaultFn];

					const expected = nativeSwitch(...scenario);
					const defaultCallCount = defaultFn.mock.calls.length;
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate, caseValue, defaultFn) {
			return hctiws(query)
				.case(candidate).then(caseValue)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate, caseValue, defaultFn) {
			let value;
			switch (query) {
			case candidate:
				value = caseValue;
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDo-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate, caseValue, defaultValue]) => {
					const caseFn = mock.fn(() => caseValue);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate, caseFn, defaultFn];

					const expected = nativeSwitch(...scenario);
					const caseCallCount = caseFn.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					caseFn.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn.mock.calls.length, caseCallCount);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate, caseFn, defaultFn) {
			return hctiws(query)
				.case(candidate).thenDo(caseFn)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate, caseFn, defaultFn) {
			let value;
			switch (query) {
			case candidate:
				value = caseFn();
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDoFallthrough-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate, caseValue, defaultValue]) => {
					const caseFn = mock.fn(() => caseValue);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate, caseFn, defaultFn];

					const expected = nativeSwitch(...scenario);
					const caseCallCount = caseFn.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					caseFn.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn.mock.calls.length, caseCallCount);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate, caseFn, defaultFn) {
			return hctiws(query)
				.case(candidate).thenDoFallthrough(caseFn)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate, caseFn, defaultFn) {
			let value;
			switch (query) {
			case candidate:
				value = caseFn();
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-then-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate, caseValue, defaultValue]) => {
					const candidateFn = mock.fn(() => candidate);
					const scenario = [query, candidateFn, caseValue, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount = candidateFn.mock.calls.length;
					candidateFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn.mock.calls.length, candidateCallCount);
				},
			),
		);

		function librarySwitch(query, candidateFn, caseValue, defaultValue) {
			return hctiws(query)
				.caseLazy(candidateFn).then(caseValue)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidateFn, caseValue, defaultValue) {
			let value;
			switch (query) {
			case candidateFn():
				value = caseValue;
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDo-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate, caseValue, defaultValue]) => {
					const candidateFn = mock.fn(() => candidate);
					const caseFn = mock.fn(() => caseValue);
					const scenario = [query, candidateFn, caseFn, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount = candidateFn.mock.calls.length;
					const caseCallCount = caseFn.mock.calls.length;
					candidateFn.mock.resetCalls();
					caseFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn.mock.calls.length, candidateCallCount);
					assert.equal(caseFn.mock.calls.length, caseCallCount);
				},
			),
		);

		function librarySwitch(query, candidateFn, caseFn, defaultValue) {
			return hctiws(query)
				.caseLazy(candidateFn).thenDo(caseFn)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidateFn, caseFn, defaultValue) {
			let value;
			switch (query) {
			case candidateFn():
				value = caseFn();
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDoFallthrough-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate, caseValue, defaultValue]) => {
					const candidateFn = mock.fn(() => candidate);
					const caseFn = mock.fn(() => caseValue);
					const scenario = [query, candidateFn, caseFn, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount = candidateFn.mock.calls.length;
					const caseCallCount = caseFn.mock.calls.length;
					candidateFn.mock.resetCalls();
					caseFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn.mock.calls.length, candidateCallCount);
					assert.equal(caseFn.mock.calls.length, caseCallCount);
				},
			),
		);

		function librarySwitch(query, candidateFn, caseFn, defaultValue) {
			return hctiws(query)
				.caseLazy(candidateFn).thenDoFallthrough(caseFn)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidateFn, caseFn, defaultValue) {
			let value;
			switch (query) {
			case candidateFn():
				value = caseFn();
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-then-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate, caseValue, defaultValue]) => {
					const candidateFn = mock.fn(() => candidate);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidateFn, caseValue, defaultFn];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount = candidateFn.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					candidateFn.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn.mock.calls.length, candidateCallCount);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidateFn, caseValue, defaultFn) {
			return hctiws(query)
				.caseLazy(candidateFn).then(caseValue)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidateFn, caseValue, defaultFn) {
			let value;
			switch (query) {
			case candidateFn():
				value = caseValue;
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDo-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate, caseValue, defaultValue]) => {
					const candidateFn = mock.fn(() => candidate);
					const caseFn = mock.fn(() => caseValue);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidateFn, caseFn, defaultFn];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount = candidateFn.mock.calls.length;
					const caseCallCount = caseFn.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					candidateFn.mock.resetCalls();
					caseFn.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn.mock.calls.length, candidateCallCount);
					assert.equal(caseFn.mock.calls.length, caseCallCount);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidateFn, caseFn, defaultFn) {
			return hctiws(query)
				.caseLazy(candidateFn).thenDo(caseFn)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidateFn, caseFn, defaultFn) {
			let value;
			switch (query) {
			case candidateFn():
				value = caseFn();
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDoFallthrough-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate, caseValue, defaultValue]) => {
					const candidateFn = mock.fn(() => candidate);
					const caseFn = mock.fn(() => caseValue);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidateFn, caseFn, defaultFn];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount = candidateFn.mock.calls.length;
					const caseCallCount = caseFn.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					candidateFn.mock.resetCalls();
					caseFn.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn.mock.calls.length, candidateCallCount);
					assert.equal(caseFn.mock.calls.length, caseCallCount);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidateFn, caseFn, defaultFn) {
			return hctiws(query)
				.caseLazy(candidateFn).thenDoFallthrough(caseFn)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidateFn, caseFn, defaultFn) {
			let value;
			switch (query) {
			case candidateFn():
				value = caseFn();
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});
});

test("switch-case-case-default", async (t) => {
	const arbScenario = () => fc.oneof(
		fc.tuple(arbQuery(), arbQuery(), arbValue(), arbQuery(), arbValue(), arbValue()),
		fc.tuple(arbQuery(), arbValue(), arbQuery(), arbValue(), arbValue())
			.map(([query, caseValue1, candidate2, caseValue2, defaultValue]) => [
				query,
				query,
				caseValue1,
				candidate2,
				caseValue2,
				defaultValue
			]),
		fc.tuple(arbQuery(), arbQuery(), arbValue(), arbValue(), arbValue())
			.map(([query, candidate1, caseValue1, caseValue2, defaultValue]) => [
				query,
				candidate1,
				caseValue1,
				query,
				caseValue2,
				defaultValue
			]),
	);

	await t.test("switch-case-then-case-then-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				(scenario) => {
					const expected = nativeSwitch(...scenario);
					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseValue2, defaultValue) {
			return hctiws(query)
				.case(candidate1).then(caseValue1)
				.case(candidate2).then(caseValue2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseValue2, defaultValue) {
			let value;
			switch (query) {
			case candidate1:
				value = caseValue1;
				break;
			case candidate2:
				value = caseValue2;
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDo-case-then-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const scenario = [query, candidate1, caseFn1, candidate2, caseValue2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					caseFn1.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseValue2, defaultValue) {
			return hctiws(query)
				.case(candidate1).thenDo(caseFn1)
				.case(candidate2).then(caseValue2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseValue2, defaultValue) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
				break;
			case candidate2:
				value = caseValue2;
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDoFallthrough-case-then-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const scenario = [query, candidate1, caseFn1, candidate2, caseValue2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					caseFn1.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseValue2, defaultValue) {
			return hctiws(query)
				.case(candidate1).thenDoFallthrough(caseFn1)
				.case(candidate2).then(caseValue2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseValue2, defaultValue) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
			case candidate2:
				value = caseValue2;
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-then-case-thenDo-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidate1, caseValue1, candidate2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const caseCallCount2 = caseFn2.mock.calls.length;
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.case(candidate1).then(caseValue1)
				.case(candidate2).thenDo(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1:
				value = caseValue1;
				break;
			case candidate2:
				value = caseFn2();
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-then-case-thenDoFallthrough-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidate1, caseValue1, candidate2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const caseCallCount2 = caseFn2.mock.calls.length;
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.case(candidate1).then(caseValue1)
				.case(candidate2).thenDoFallthrough(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1:
				value = caseValue1;
				break;
			case candidate2:
				value = caseFn2();
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-then-case-then-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate1, caseValue1, candidate2, caseValue2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const defaultCallCount = defaultFn.mock.calls.length;
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseValue2, defaultFn) {
			return hctiws(query)
				.case(candidate1).then(caseValue1)
				.case(candidate2).then(caseValue2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseValue2, defaultFn) {
			let value;
			switch (query) {
			case candidate1:
				value = caseValue1;
				break;
			case candidate2:
				value = caseValue2;
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDo-case-thenDo-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidate1, caseFn1, candidate2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					caseFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.case(candidate1).thenDo(caseFn1)
				.case(candidate2).thenDo(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
				break;
			case candidate2:
				value = caseFn2();
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDoFallthrough-case-thenDo-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidate1, caseFn1, candidate2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					caseFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.case(candidate1).thenDoFallthrough(caseFn1)
				.case(candidate2).thenDo(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
			case candidate2:
				value = caseFn2();
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDo-case-thenDoFallthrough-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidate1, caseFn1, candidate2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					caseFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.case(candidate1).thenDo(caseFn1)
				.case(candidate2).thenDoFallthrough(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
				break;
			case candidate2:
				value = caseFn2();
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDoFallthrough-case-thenDoFallthrough-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidate1, caseFn1, candidate2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					caseFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.case(candidate1).thenDoFallthrough(caseFn1)
				.case(candidate2).thenDoFallthrough(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
			case candidate2:
				value = caseFn2();
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDo-case-then-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate1, caseFn1, candidate2, caseValue2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					caseFn1.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseValue, defaultFn) {
			return hctiws(query)
				.case(candidate1).thenDo(caseFn1)
				.case(candidate2).then(caseValue)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseValue, defaultFn) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
				break;
			case candidate2:
				value = caseValue;
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDoFallthrough-case-then-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate1, caseFn1, candidate2, caseValue2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					caseFn1.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseValue, defaultFn) {
			return hctiws(query)
				.case(candidate1).thenDoFallthrough(caseFn1)
				.case(candidate2).then(caseValue)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseValue, defaultFn) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
			case candidate2:
				value = caseValue;
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-then-case-thenDo-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate1, caseValue1, candidate2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const caseCallCount2 = caseFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					caseFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.case(candidate1).then(caseValue1)
				.case(candidate2).thenDo(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1:
				value = caseValue1;
				break;
			case candidate2:
				value = caseFn2();
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-then-case-thenDoFallthrough-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate1, caseValue1, candidate2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const caseCallCount2 = caseFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					caseFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.case(candidate1).then(caseValue1)
				.case(candidate2).thenDoFallthrough(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1:
				value = caseValue1;
				break;
			case candidate2:
				value = caseFn2();
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDo-case-thenDo-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate1, caseFn1, candidate2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					caseFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.case(candidate1).thenDo(caseFn1)
				.case(candidate2).thenDo(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
				break;
			case candidate2:
				value = caseFn2();
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDoFallthrough-case-thenDo-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate1, caseFn1, candidate2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					caseFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.case(candidate1).thenDoFallthrough(caseFn1)
				.case(candidate2).thenDo(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
			case candidate2:
				value = caseFn2();
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDo-case-thenDoFallthrough-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate1, caseFn1, candidate2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					caseFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.case(candidate1).thenDo(caseFn1)
				.case(candidate2).thenDoFallthrough(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
				break;
			case candidate2:
				value = caseFn2();
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDoFallthrough-case-thenDoFallthrough-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate1, caseFn1, candidate2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					caseFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.case(candidate1).thenDoFallthrough(caseFn1)
				.case(candidate2).thenDoFallthrough(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
			case candidate2:
				value = caseFn2();
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-then-case-then-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const scenario = [query, candidateFn1, caseValue1, candidate2, caseValue2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					candidateFn1.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseValue2, defaultValue) {
			return hctiws(query)
				.caseLazy(candidate1).then(caseValue1)
				.case(candidate2).then(caseValue2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseValue2, defaultValue) {
			let value;
			switch (query) {
			case candidate1():
				value = caseValue1;
				break;
			case candidate2:
				value = caseValue2;
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-then-caseLazy-then-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn2 = mock.fn(() => candidate2);
					const scenario = [query, candidate1, caseValue1, candidateFn2, caseValue2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					candidateFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseValue2, defaultValue) {
			return hctiws(query)
				.case(candidate1).then(caseValue1)
				.caseLazy(candidate2).then(caseValue2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseValue2, defaultValue) {
			let value;
			switch (query) {
			case candidate1:
				value = caseValue1;
				break;
			case candidate2():
				value = caseValue2;
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-then-caseLazy-then-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const candidateFn2 = mock.fn(() => candidate2);
					const scenario = [query, candidateFn1, caseValue1, candidateFn2, caseValue2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					candidateFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseValue2, defaultValue) {
			return hctiws(query)
				.caseLazy(candidate1).then(caseValue1)
				.caseLazy(candidate2).then(caseValue2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseValue2, defaultValue) {
			let value;
			switch (query) {
			case candidate1():
				value = caseValue1;
				break;
			case candidate2():
				value = caseValue2;
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDo-case-then-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const scenario = [query, candidateFn1, caseFn1, candidate2, caseValue2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseValue2, defaultValue) {
			return hctiws(query)
				.caseLazy(candidate1).thenDo(caseFn1)
				.case(candidate2).then(caseValue2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseValue2, defaultValue) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
				break;
			case candidate2:
				value = caseValue2;
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDo-caseLazy-then-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const scenario = [query, candidate1, caseFn1, candidateFn2, caseValue2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseValue2, defaultValue) {
			return hctiws(query)
				.case(candidate1).thenDo(caseFn1)
				.caseLazy(candidate2).then(caseValue2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseValue2, defaultValue) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
				break;
			case candidate2():
				value = caseValue2;
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDo-caseLazy-then-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const scenario = [query, candidateFn1, caseFn1, candidateFn2, caseValue2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseValue2, defaultValue) {
			return hctiws(query)
				.caseLazy(candidate1).thenDo(caseFn1)
				.caseLazy(candidate2).then(caseValue2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseValue2, defaultValue) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
				break;
			case candidate2():
				value = caseValue2;
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDoFallthrough-case-then-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const scenario = [query, candidateFn1, caseFn1, candidate2, caseValue2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseValue2, defaultValue) {
			return hctiws(query)
				.caseLazy(candidate1).thenDoFallthrough(caseFn1)
				.case(candidate2).then(caseValue2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseValue2, defaultValue) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
			case candidate2:
				value = caseValue2;
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDoFallthrough-caseLazy-then-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const scenario = [query, candidate1, caseFn1, candidateFn2, caseValue2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseValue2, defaultValue) {
			return hctiws(query)
				.case(candidate1).thenDoFallthrough(caseFn1)
				.caseLazy(candidate2).then(caseValue2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseValue2, defaultValue) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
			case candidate2():
				value = caseValue2;
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDoFallthrough-caseLazy-then-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const scenario = [query, candidateFn1, caseFn1, candidateFn2, caseValue2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseValue2, defaultValue) {
			return hctiws(query)
				.caseLazy(candidate1).thenDoFallthrough(caseFn1)
				.caseLazy(candidate2).then(caseValue2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseValue2, defaultValue) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
			case candidate2():
				value = caseValue2;
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-then-case-thenDo-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidateFn1, caseValue1, candidate2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.caseLazy(candidate1).then(caseValue1)
				.case(candidate2).thenDo(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1():
				value = caseValue1;
				break;
			case candidate2:
				value = caseFn2();
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-then-caseLazy-thenDo-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn2 = mock.fn(() => caseValue2);
					const candidateFn2 = mock.fn(() => candidate2);
					const scenario = [query, candidate1, caseValue1, candidateFn2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const caseCallCount2 = caseFn2.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					caseFn2.mock.resetCalls();
					candidateFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.case(candidate1).then(caseValue1)
				.caseLazy(candidate2).thenDo(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1:
				value = caseValue1;
				break;
			case candidate2():
				value = caseFn2();
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-then-caseLazy-thenDo-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn2 = mock.fn(() => caseValue2);
					const candidateFn2 = mock.fn(() => candidate2);
					const scenario = [query, candidateFn1, caseValue1, candidateFn2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();
					candidateFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.caseLazy(candidate1).then(caseValue1)
				.caseLazy(candidate2).thenDo(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1():
				value = caseValue1;
				break;
			case candidate2():
				value = caseFn2();
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-then-case-thenDoFallthrough-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidateFn1, caseValue1, candidate2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.caseLazy(candidate1).then(caseValue1)
				.case(candidate2).thenDoFallthrough(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1():
				value = caseValue1;
				break;
			case candidate2:
				value = caseFn2();
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-then-caseLazy-thenDoFallthrough-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn2 = mock.fn(() => caseValue2);
					const candidateFn2 = mock.fn(() => candidate2);
					const scenario = [query, candidate1, caseValue1, candidateFn2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const caseCallCount2 = caseFn2.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					caseFn2.mock.resetCalls();
					candidateFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.case(candidate1).then(caseValue1)
				.caseLazy(candidate2).thenDoFallthrough(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1:
				value = caseValue1;
				break;
			case candidate2():
				value = caseFn2();
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-then-caseLazy-thenDoFallthrough-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn2 = mock.fn(() => caseValue2);
					const candidateFn2 = mock.fn(() => candidate2);
					const scenario = [query, candidateFn1, caseValue1, candidateFn2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();
					candidateFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.caseLazy(candidate1).then(caseValue1)
				.caseLazy(candidate2).thenDoFallthrough(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1():
				value = caseValue1;
				break;
			case candidate2():
				value = caseFn2();
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-then-case-then-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidateFn1, caseValue1, candidate2, caseValue2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					candidateFn1.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseValue2, defaultFn) {
			return hctiws(query)
				.caseLazy(candidate1).then(caseValue1)
				.case(candidate2).then(caseValue2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseValue2, defaultFn) {
			let value;
			switch (query) {
			case candidate1():
				value = caseValue1;
				break;
			case candidate2:
				value = caseValue2;
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-then-caseLazy-then-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn2 = mock.fn(() => candidate2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate1, caseValue1, candidateFn2, caseValue2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const defaultCallCount = defaultFn.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					defaultFn.mock.resetCalls();
					candidateFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseValue2, defaultFn) {
			return hctiws(query)
				.case(candidate1).then(caseValue1)
				.caseLazy(candidate2).then(caseValue2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseValue2, defaultFn) {
			let value;
			switch (query) {
			case candidate1:
				value = caseValue1;
				break;
			case candidate2():
				value = caseValue2;
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-then-caseLazy-then-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const candidateFn2 = mock.fn(() => candidate2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidateFn1, caseValue1, candidateFn2, caseValue2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					candidateFn1.mock.resetCalls();
					defaultFn.mock.resetCalls();
					candidateFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseValue2, defaultFn) {
			return hctiws(query)
				.caseLazy(candidate1).then(caseValue1)
				.caseLazy(candidate2).then(caseValue2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseValue2, defaultFn) {
			let value;
			switch (query) {
			case candidate1():
				value = caseValue1;
				break;
			case candidate2():
				value = caseValue2;
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDo-case-thenDo-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidateFn1, caseFn1, candidate2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.caseLazy(candidate1).thenDo(caseFn1)
				.case(candidate2).thenDo(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
				break;
			case candidate2:
				value = caseFn2();
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDo-caseLazy-thenDo-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidate1, caseFn1, candidateFn2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.case(candidate1).thenDo(caseFn1)
				.caseLazy(candidate2).thenDo(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
				break;
			case candidate2():
				value = caseFn2();
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDo-caseLazy-thenDo-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidateFn1, caseFn1, candidateFn2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.caseLazy(candidate1).thenDo(caseFn1)
				.caseLazy(candidate2).thenDo(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
				break;
			case candidate2():
				value = caseFn2();
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDoFallthrough-case-thenDo-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidateFn1, caseFn1, candidate2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.caseLazy(candidate1).thenDoFallthrough(caseFn1)
				.case(candidate2).thenDo(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
			case candidate2:
				value = caseFn2();
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDoFallthrough-caseLazy-thenDo-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidate1, caseFn1, candidateFn2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.case(candidate1).thenDoFallthrough(caseFn1)
				.caseLazy(candidate2).thenDo(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
			case candidate2():
				value = caseFn2();
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDoFallthrough-caseLazy-thenDo-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidateFn1, caseFn1, candidateFn2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.caseLazy(candidate1).thenDoFallthrough(caseFn1)
				.caseLazy(candidate2).thenDo(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
			case candidate2():
				value = caseFn2();
				break;
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDo-case-thenDoFallthrough-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidateFn1, caseFn1, candidate2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.caseLazy(candidate1).thenDo(caseFn1)
				.case(candidate2).thenDoFallthrough(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
				break;
			case candidate2:
				value = caseFn2();
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDo-caseLazy-thenDoFallthrough-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidate1, caseFn1, candidateFn2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.case(candidate1).thenDo(caseFn1)
				.caseLazy(candidate2).thenDoFallthrough(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
				break;
			case candidate2():
				value = caseFn2();
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDo-caseLazy-thenDoFallthrough-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidateFn1, caseFn1, candidateFn2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.caseLazy(candidate1).thenDo(caseFn1)
				.caseLazy(candidate2).thenDoFallthrough(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
				break;
			case candidate2():
				value = caseFn2();
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDoFallthrough-case-thenDoFallthrough-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidateFn1, caseFn1, candidate2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.caseLazy(candidate1).thenDoFallthrough(caseFn1)
				.case(candidate2).thenDoFallthrough(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
			case candidate2:
				value = caseFn2();
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDoFallthrough-caseLazy-thenDoFallthrough-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidate1, caseFn1, candidateFn2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.case(candidate1).thenDoFallthrough(caseFn1)
				.caseLazy(candidate2).thenDoFallthrough(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
			case candidate2():
				value = caseFn2();
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDoFallthrough-caseLazy-thenDoFallthrough-default", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const caseFn2 = mock.fn(() => caseValue2);
					const scenario = [query, candidateFn1, caseFn1, candidateFn2, caseFn2, defaultValue];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					caseFn2.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			return hctiws(query)
				.caseLazy(candidate1).thenDoFallthrough(caseFn1)
				.caseLazy(candidate2).thenDoFallthrough(caseFn2)
				.default(defaultValue);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultValue) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
			case candidate2():
				value = caseFn2();
			default:
				value = defaultValue;
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDo-case-then-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidateFn1, caseFn1, candidate2, caseValue2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseValue, defaultFn) {
			return hctiws(query)
				.caseLazy(candidate1).thenDo(caseFn1)
				.case(candidate2).then(caseValue)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseValue, defaultFn) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
				break;
			case candidate2:
				value = caseValue;
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDo-caseLazy-then-defaultDo", async () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate1, caseFn1, candidateFn2, caseValue2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseValue, defaultFn) {
			return hctiws(query)
				.case(candidate1).thenDo(caseFn1)
				.caseLazy(candidate2).then(caseValue)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseValue, defaultFn) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
				break;
			case candidate2():
				value = caseValue;
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDo-caseLazy-then-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidateFn1, caseFn1, candidateFn2, caseValue2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseValue, defaultFn) {
			return hctiws(query)
				.caseLazy(candidate1).thenDo(caseFn1)
				.caseLazy(candidate2).then(caseValue)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseValue, defaultFn) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
				break;
			case candidate2():
				value = caseValue;
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDoFallthrough-case-then-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidateFn1, caseFn1, candidate2, caseValue2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseValue, defaultFn) {
			return hctiws(query)
				.caseLazy(candidate1).thenDoFallthrough(caseFn1)
				.case(candidate2).then(caseValue)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseValue, defaultFn) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
			case candidate2:
				value = caseValue;
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDoFallthrough-caseLazy-then-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate1, caseFn1, candidateFn2, caseValue2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseValue, defaultFn) {
			return hctiws(query)
				.case(candidate1).thenDoFallthrough(caseFn1)
				.caseLazy(candidate2).then(caseValue)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseValue, defaultFn) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
			case candidate2():
				value = caseValue;
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDoFallthrough-caseLazy-then-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidateFn1, caseFn1, candidateFn2, caseValue2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseValue, defaultFn) {
			return hctiws(query)
				.caseLazy(candidate1).thenDoFallthrough(caseFn1)
				.caseLazy(candidate2).then(caseValue)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseValue, defaultFn) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
			case candidate2():
				value = caseValue;
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-then-case-thenDo-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidateFn1, caseValue1, candidate2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.caseLazy(candidate1).then(caseValue1)
				.case(candidate2).thenDo(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1():
				value = caseValue1;
				break;
			case candidate2:
				value = caseFn2();
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-then-caseLazy-thenDo-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn2 = mock.fn(() => candidate2);
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate1, caseValue1, candidateFn2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const caseCallCount2 = caseFn2.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					caseFn2.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.case(candidate1).then(caseValue1)
				.caseLazy(candidate2).thenDo(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1:
				value = caseValue1;
				break;
			case candidate2():
				value = caseFn2();
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-then-caseLazy-thenDo-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const candidateFn2 = mock.fn(() => candidate2);
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidateFn1, caseValue1, candidateFn2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.caseLazy(candidate1).then(caseValue1)
				.caseLazy(candidate2).thenDo(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1():
				value = caseValue1;
				break;
			case candidate2():
				value = caseFn2();
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-then-case-thenDoFallthrough-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidateFn1, caseValue1, candidate2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.caseLazy(candidate1).then(caseValue1)
				.case(candidate2).thenDoFallthrough(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1():
				value = caseValue1;
				break;
			case candidate2:
				value = caseFn2();
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-then-caseLazy-thenDoFallthrough-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn2 = mock.fn(() => candidate2);
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate1, caseValue1, candidateFn2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const caseCallCount2 = caseFn2.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					caseFn2.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.case(candidate1).then(caseValue1)
				.caseLazy(candidate2).thenDoFallthrough(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1:
				value = caseValue1;
				break;
			case candidate2():
				value = caseFn2();
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-then-caseLazy-thenDoFallthrough-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const candidateFn2 = mock.fn(() => candidate2);
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidateFn1, caseValue1, candidateFn2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.caseLazy(candidate1).then(caseValue1)
				.caseLazy(candidate2).thenDoFallthrough(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseValue1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1():
				value = caseValue1;
				break;
			case candidate2():
				value = caseFn2();
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDo-case-thenDo-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidateFn1, caseFn1, candidate2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.caseLazy(candidate1).thenDo(caseFn1)
				.case(candidate2).thenDo(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
				break;
			case candidate2:
				value = caseFn2();
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDo-caseLazy-thenDo-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate1, caseFn1, candidateFn2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					caseFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.case(candidate1).thenDo(caseFn1)
				.caseLazy(candidate2).thenDo(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
				break;
			case candidate2():
				value = caseFn2();
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDo-caseLazy-thenDo-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidateFn1, caseFn1, candidateFn2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					caseFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.caseLazy(candidate1).thenDo(caseFn1)
				.caseLazy(candidate2).thenDo(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
				break;
			case candidate2():
				value = caseFn2();
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDoFallthrough-case-thenDo-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidateFn1, caseFn1, candidate2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.caseLazy(candidate1).thenDoFallthrough(caseFn1)
				.case(candidate2).thenDo(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
			case candidate2:
				value = caseFn2();
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDoFallthrough-caseLazy-thenDo-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate1, caseFn1, candidateFn2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					caseFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.case(candidate1).thenDoFallthrough(caseFn1)
				.caseLazy(candidate2).thenDo(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
			case candidate2():
				value = caseFn2();
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDoFallthrough-caseLazy-thenDo-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidateFn1, caseFn1, candidateFn2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					caseFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.caseLazy(candidate1).thenDoFallthrough(caseFn1)
				.caseLazy(candidate2).thenDo(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
			case candidate2():
				value = caseFn2();
				break;
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDo-case-thenDoFallthrough-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidateFn1, caseFn1, candidate2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					caseFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.caseLazy(candidate1).thenDo(caseFn1)
				.case(candidate2).thenDoFallthrough(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
				break;
			case candidate2:
				value = caseFn2();
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-case-thenDo-caseLazy-thenDoFallthrough-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidate1, caseFn1, candidateFn2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					caseFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.case(candidate1).thenDo(caseFn1)
				.caseLazy(candidate2).thenDoFallthrough(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1:
				value = caseFn1();
				break;
			case candidate2():
				value = caseFn2();
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});

	await t.test("switch-caseLazy-thenDo-caseLazy-thenDoFallthrough-defaultDo", () => {
		fc.assert(
			fc.property(
				arbScenario(),
				([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => {
					const candidateFn1 = mock.fn(() => candidate1);
					const caseFn1 = mock.fn(() => caseValue1);
					const candidateFn2 = mock.fn(() => candidate2);
					const caseFn2 = mock.fn(() => caseValue2);
					const defaultFn = mock.fn(() => defaultValue);
					const scenario = [query, candidateFn1, caseFn1, candidateFn2, caseFn2, defaultFn];

					const expected = nativeSwitch(...scenario);
					const candidateCallCount1 = candidateFn1.mock.calls.length;
					const caseCallCount1 = caseFn1.mock.calls.length;
					const candidateCallCount2 = candidateFn2.mock.calls.length;
					const caseCallCount2 = caseFn2.mock.calls.length;
					const defaultCallCount = defaultFn.mock.calls.length;
					candidateFn1.mock.resetCalls();
					caseFn1.mock.resetCalls();
					candidateFn2.mock.resetCalls();
					caseFn2.mock.resetCalls();
					defaultFn.mock.resetCalls();

					const actual = librarySwitch(...scenario);
					assert.equal(actual, expected);
					assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					assert.equal(caseFn1.mock.calls.length, caseCallCount1);
					assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					assert.equal(caseFn2.mock.calls.length, caseCallCount2);
					assert.equal(defaultFn.mock.calls.length, defaultCallCount);
				},
			),
		);

		function librarySwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			return hctiws(query)
				.caseLazy(candidate1).thenDo(caseFn1)
				.caseLazy(candidate2).thenDoFallthrough(caseFn2)
				.defaultDo(defaultFn);
		}

		function nativeSwitch(query, candidate1, caseFn1, candidate2, caseFn2, defaultFn) {
			let value;
			switch (query) {
			case candidate1():
				value = caseFn1();
				break;
			case candidate2():
				value = caseFn2();
			default:
				value = defaultFn();
				break;
			}

			return value;
		}
	});
});

test("multi-valued case", async (t) => {
	await t.test("switch-case-default", async (t) => {
		const arbScenario = () => fc.oneof(
			fc.tuple(arbQuery(), arbQuery(), arbQuery(), arbValue(), arbValue()),
			fc.tuple(arbQuery(), arbQuery(), arbValue(), arbValue())
				.map(([query, candidate2, caseValue, defaultValue]) => [
					query,
					query,
					candidate2,
					caseValue,
					defaultValue,
				]),
			fc.tuple(arbQuery(), arbQuery(), arbValue(), arbValue())
				.map(([query, candidate1, caseValue, defaultValue]) => [
					query,
					candidate1,
					query,
					caseValue,
					defaultValue,
				]),
		);

		await t.test("switch-case-then-default", () => {
			fc.assert(
				fc.property(
					arbScenario(),
					(scenario) => {
						const expected = nativeSwitch(...scenario);
						const actual = librarySwitch(...scenario);
						assert.equal(actual, expected);
					},
				),
			);

			function librarySwitch(query, candidate1, candidate2, caseValue, defaultValue) {
				return hctiws(query)
					.case(candidate1, candidate2).then(caseValue)
					.default(defaultValue);
			}

			function nativeSwitch(query, candidate1, candidate2, caseValue, defaultValue) {
				let value;
				switch (query) {
				case candidate1:
				case candidate2:
					value = caseValue;
					break;
				default:
					value = defaultValue;
					break;
				}

				return value;
			}
		});

		await t.test("switch-caseLazy-then-default", () => {
			fc.assert(
				fc.property(
					arbScenario(),
					([query, candidate1, candidate2, caseValue1, defaultValue]) => {
						const candidateFn = mock.fn(() => [candidate1, candidate2]);
						const scenario = [query, candidateFn, caseValue1, defaultValue];

						const expected = nativeSwitch(...scenario);
						const actual = librarySwitch(...scenario);
						assert.equal(actual, expected);
					},
				),
			);

			function librarySwitch(query, candidateFn, caseValue, defaultValue) {
				return hctiws(query)
					.caseLazy(candidateFn).then(caseValue)
					.default(defaultValue);
			}

			function nativeSwitch(query, candidateFn, caseValue, defaultValue) {
				let value;
				switch (query) {
				case candidateFn().find(candidate => query === candidate):
					value = caseValue;
					break;
				default:
					value = defaultValue;
					break;
				}

				return value;
			}
		});
	});

	await t.test("switch-case-case-default", async (t) => {
		const arbScenario = () => fc.oneof(
			fc.tuple(arbQuery(), arbQuery(), arbValue(), arbQuery(), arbQuery(), arbValue(), arbValue()),
			fc.tuple(arbQuery(), arbValue(), arbQuery(), arbQuery(), arbValue(), arbValue())
				.map(([query, caseValue1, candidate2, candidate3, caseValue2, defaultValue]) => [
					query,
					query,
					caseValue1,
					candidate2,
					candidate3,
					caseValue2,
					defaultValue,
				]),
			fc.tuple(arbQuery(), arbQuery(), arbValue(), arbQuery(), arbValue(), arbValue())
				.map(([query, candidate1, caseValue1, candidate3, caseValue2, defaultValue]) => [
					query,
					candidate1,
					caseValue1,
					query,
					candidate3,
					caseValue2,
					defaultValue,
				]),
			fc.tuple(arbQuery(), arbQuery(), arbValue(), arbQuery(), arbValue(), arbValue())
				.map(([query, candidate1, caseValue1, candidate2, caseValue2, defaultValue]) => [
					query,
					candidate1,
					caseValue1,
					candidate2,
					query,
					caseValue2,
					defaultValue,
				]),
		);

		await t.test("switch-case-then-case-then-default", () => {
			fc.assert(
				fc.property(
					arbScenario(),
					(scenario) => {
						const expected = nativeSwitch(...scenario);
						const actual = librarySwitch(...scenario);
						assert.equal(actual, expected);
					},
				),
			);

			function librarySwitch(
				query, candidate1, caseValue1, candidate2, candidate3, caseValue2, defaultValue
			) {
				return hctiws(query)
					.case(candidate1).then(caseValue1)
					.case(candidate2, candidate3).then(caseValue2)
					.default(defaultValue);
			}

			function nativeSwitch(
				query, candidate1, caseValue1, candidate2, candidate3, caseValue2, defaultValue
			) {
				let value;
				switch (query) {
				case candidate1:
					value = caseValue1;
					break;
				case candidate2:
				case candidate3:
					value = caseValue2;
					break;
				default:
					value = defaultValue;
					break;
				}

				return value;
			}
		});

		await t.test("switch-caseLazy-then-case-then-default", () => {
			fc.assert(
				fc.property(
					arbScenario(),
					([query, candidate1, caseValue1, candidate2, candidate3, caseValue2, defaultValue]) => {
						const candidateFn1 = mock.fn(() => candidate1);
						const scenario = [
							query, candidateFn1, caseValue1, candidate2, candidate3, caseValue2, defaultValue
						];

						const expected = nativeSwitch(...scenario);
						const candidateCallCount1 = candidateFn1.mock.calls.length;
						candidateFn1.mock.resetCalls();

						const actual = librarySwitch(...scenario);
						assert.equal(actual, expected);
						assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
					},
				),
			);

			function librarySwitch(
				query, candidateFn1, caseValue1, candidate2, candidate3, caseValue2, defaultValue
			) {
				return hctiws(query)
					.caseLazy(candidateFn1).then(caseValue1)
					.case(candidate2, candidate3).then(caseValue2)
					.default(defaultValue);
			}

			function nativeSwitch(
				query, candidateFn1, caseValue1, candidate2, candidate3, caseValue2, defaultValue
			) {
				let value;
				switch (query) {
				case candidateFn1():
					value = caseValue1;
					break;
				case candidate2:
				case candidate3:
					value = caseValue2;
					break;
				default:
					value = defaultValue;
					break;
				}

				return value;
			}
		});

		await t.test("switch-case-then-caseLazy-then-default", () => {
			fc.assert(
				fc.property(
					arbScenario(),
					([query, candidate1, caseValue1, candidate2, candidate3, caseValue2, defaultValue]) => {
						const candidateFn2 = mock.fn(() => [candidate2, candidate3]);
						const scenario = [query, candidate1, caseValue1, candidateFn2, caseValue2, defaultValue];

						const expected = nativeSwitch(...scenario);
						const candidateCallCount2 = candidateFn2.mock.calls.length;
						candidateFn2.mock.resetCalls();

						const actual = librarySwitch(...scenario);
						assert.equal(actual, expected);
						assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					},
				),
			);

			function librarySwitch(query, candidate1, caseValue1, candidateFn2, caseValue2, defaultValue) {
				return hctiws(query)
					.case(candidate1).then(caseValue1)
					.caseLazy(candidateFn2).then(caseValue2)
					.default(defaultValue);
			}

			function nativeSwitch(query, candidate1, caseValue1, candidateFn2, caseValue2, defaultValue) {
				let value;
				switch (query) {
				case candidate1:
					value = caseValue1;
					break;
				case candidateFn2().find(candidate => query === candidate):
					value = caseValue2;
					break;
				default:
					value = defaultValue;
					break;
				}

				return value;
			}
		});

		await t.test("switch-caseLazy-then-caseLazy-then-default", () => {
			fc.assert(
				fc.property(
					arbScenario(),
					([query, candidate1, caseValue1, candidate2, candidate3, caseValue2, defaultValue]) => {
						const candidateFn1 = mock.fn(() => candidate1);
						const candidateFn2 = mock.fn(() => [candidate2, candidate3]);
						const scenario = [query, candidateFn1, caseValue1, candidateFn2, caseValue2, defaultValue];

						const expected = nativeSwitch(...scenario);
						const candidateCallCount1 = candidateFn1.mock.calls.length;
						const candidateCallCount2 = candidateFn2.mock.calls.length;
						candidateFn1.mock.resetCalls();
						candidateFn2.mock.resetCalls();

						const actual = librarySwitch(...scenario);
						assert.equal(actual, expected);
						assert.equal(candidateFn1.mock.calls.length, candidateCallCount1);
						assert.equal(candidateFn2.mock.calls.length, candidateCallCount2);
					},
				),
			);

			function librarySwitch(
				query, candidateFn1, caseValue1, candidateFn2, caseValue2, defaultValue
			) {
				return hctiws(query)
					.caseLazy(candidateFn1).then(caseValue1)
					.caseLazy(candidateFn2).then(caseValue2)
					.default(defaultValue);
			}

			function nativeSwitch(query, candidateFn1, caseValue1, candidateFn2, caseValue2, defaultValue) {
				let value;
				switch (query) {
				case candidateFn1():
					value = caseValue1;
					break;
				case candidateFn2().find(candidate => query === candidate):
					value = caseValue2;
					break;
				default:
					value = defaultValue;
					break;
				}

				return value;
			}
		});
	});
});

test("arbitrary switch expression", () => {
	class EagerCandidate {
		constructor(candidates) {
			this.candidates = candidates;
		}

		check(model) {
			return model.lastCase === null;
		}

		run(model, real) {
			model.lastCase = this.candidates.includes(model.query);
			real.expr = real.expr.case(...this.candidates);
		}

		toString() {
			return `case(${this.candidates})`;
		}
	}

	class LazyCandidate {
		constructor(candidates) {
			this.candidates = candidates;
		}

		check(model) {
			return model.lastCase === null;
		}

		run(model, real) {
			model.lastCase = this.candidates.includes(model.query);

			const fn = mock.fn(() => this.candidates);
			real.expr = real.expr.caseLazy(fn);

			assert.equal(fn.mock.calls.length, (model.result === null && !model.fallthrough) ? 1 : 0);
		}

		toString() {
			return `caseLazy(() => ${this.candidates})`;
		}
	}

	class EagerBranch {
		constructor(value) {
			this.value = value;
		}

		check(model) {
			return model.lastCase !== null;
		}

		run(model, real) {
			const branchTaken = model.result === null && (model.lastCase || model.fallthrough);

			model.lastCase = null;
			model.fallthrough = false;
			if (branchTaken) {
				model.result = this.value;
			}

			real.expr = real.expr.then(this.value);
		}

		toString() {
			return `then(${this.value})`;
		}
	}

	class LazyBranch {
		constructor(value) {
			this.value = value;
		}

		check(model) {
			return model.lastCase !== null;
		}

		run(model, real) {
			const branchTaken = model.result === null && (model.lastCase || model.fallthrough);

			model.lastCase = null;
			model.fallthrough = false;
			if (branchTaken) {
				model.result = this.value;
			}

			const fn = mock.fn(() => this.value);
			real.expr = real.expr.thenDo(fn);

			assert.equal(fn.mock.calls.length, branchTaken ? 1 : 0);
		}

		toString() {
			return `thenDo(() => ${this.value})`;
		}
	}

	class FallthroughBranch {
		constructor(value) {
			this.value = value;
		}

		check(model) {
			return model.lastCase !== null;
		}

		run(model, real) {
			const branchTaken = model.result === null && (model.lastCase || model.fallthrough);

			model.lastCase = null;
			if (branchTaken) {
				model.fallthrough = true;
			}

			const fn = mock.fn(() => this.value);
			real.expr = real.expr.thenDoFallthrough(fn);

			assert.equal(fn.mock.calls.length, branchTaken ? 1 : 0);
		}

		toString() {
			return `thenDoFallthrough(() => ${this.value})`;
		}
	}

	class EagerDefault {
		constructor(value) {
			this.value = value;
		}

		check(model) {
			return model.lastCase === null;
		}

		run(model, real) {
			const branchTaken = model.result === null;
			const expected = branchTaken ? this.value : model.result;

			const actual = real.expr.default(this.value);

			assert.equal(actual, expected);
		}

		toString() {
			return `default(${this.value})`;
		}
	}

	class LazyDefault {
		constructor(value) {
			this.value = value;
		}

		check(model) {
			return model.lastCase === null;
		}

		run(model, real) {
			const branchTaken = model.result === null;
			const expected = branchTaken ? this.value : model.result;

			const fn = mock.fn(() => this.value);
			const actual = real.expr.defaultDo(fn);

			assert.equal(actual, expected);
			assert.equal(fn.mock.calls.length, branchTaken ? 1 : 0);
		}

		toString() {
			return `defaultDo(() => ${this.value})`;
		}
	}

	const queryValue = fc.stringMatching(/^[0-9]{1,2}$/);
	const commands = [
		fc.array(queryValue, { minLength: 1, maxLength: 10 })
			.map(candidates => new EagerCandidate(candidates)),
		fc.array(queryValue, { minLength: 1, maxLength: 10 })
			.map(candidates => new LazyCandidate(candidates)),
		fc.integer().map(value => new EagerBranch(value)),
		fc.integer().map(value => new LazyBranch(value)),
		fc.integer().map(value => new FallthroughBranch(value)),
		fc.integer().map(value => new EagerDefault(value)),
		fc.integer().map(value => new LazyDefault(value)),
	];

	fc.assert(
		fc.property(
			queryValue,
			fc.commands(commands, { size: '+1' }),
			(query, commands) => {
				const setup = () => ({
					model: { fallthrough: false, lastCase: null, query, result: null },
					real: { expr: hctiws(query) },
				});

				fc.modelRun(setup, commands);
			},
		),
	);
});
