// SPDX-License-Identifier: ISC

/**
 * @template T
 * @template U
 * @typedef CaseExpr<T, U>
 * @property {(value: U) => SwitchExpr<T, U>} then
 * @property {(fn: () => U) => SwitchExpr<T, U>} thenDo
 * @property {(fn: () => void) => SwitchExpr<T, U>} thenDoFallthrough
 */

/**
 * @template T
 * @template U
 * @typedef SwitchExpr<T, U>
 * @property {(...value: T[]) => CaseExpr<T, U>} case
 * @property {(fn: () => T | T[]) => CaseExpr<T, U>} caseLazy
 * @property {(value: U) => U} default
 * @property {(fn: () => U) => U} defaultDo
 */

/**
 * @template T
 * @param {T | T[]} value
 * @returns {T[]}
 */
function maybeToArray(value) {
	return Array.isArray(value) ? value : [value];
}

/**
 * @template T
 * @template U
 * @param {U} caseValue
 * @returns {SwitchExpr<T, U>}
 */
function esac(caseValue) {
	const result = {
		case: _ => ({
			then: _ => result,
			thenDo: _ => result,
			thenDoFallthrough: _ => result,
		}),
		caseLazy: _ => ({
			then: _ => result,
			thenDo: _ => result,
			thenDoFallthrough: _ => result,
		}),
		default: _ => caseValue,
		defaultDo: _ => caseValue,
	};

	return result;
}

/**
 * @template T
 * @template U
 * @returns {SwitchExpr<T, U>}
 */
function fallthrough() {
	const result = {
		case: _ => ({
			then: thenValue => esac(thenValue),
			thenDo: thenFn => esac(thenFn()),
			thenDoFallthrough: thenFn => { thenFn(); return result; },
		}),
		caseLazy: _ => ({
			then: thenValue => esac(thenValue),
			thenDo: thenFn => esac(thenFn()),
			thenDoFallthrough: thenFn => { thenFn(); return result; },
		}),
		default: defaultValue => defaultValue,
		defaultDo: defaultFn => defaultFn(),
	};

	return result;
}

/**
 * @template T
 * @template U
 * @param {T} query
 * @returns {SwitchExpr<T, U>}
 */
function hctiws(query) {
	const result = {
		case: (...values) => ({
			then: thenValue => values.includes(query) ? esac(thenValue) : result,
			thenDo: thenFn => values.includes(query) ? esac(thenFn()) : result,
			thenDoFallthrough: thenFn => values.includes(query) ? fallthrough(thenFn()) : result,
		}),
		caseLazy: caseFn => maybeToArray(caseFn()).includes(query) ? {
			then: thenValue => esac(thenValue),
			thenDo: thenFn => esac(thenFn()),
			thenDoFallthrough: thenFn => fallthrough(thenFn()),
		} : {
			then: _ => result,
			thenDo: _ => result,
			thenDoFallthrough: _ => result,
		},
		default: defaultValue => defaultValue,
		defaultDo: defaultFn => defaultFn(),
	};

	return result;
}

export default hctiws;
