# Security Policy

The maintainers of `cont-flow-expr` take security issues seriously. We appreciate your efforts to
responsibly disclose your findings. Due to the non-funded and open-source nature of the project, we
take a best-efforts approach when it comes to engaging with security reports.

This document should be considered expired after 2025-06-01. If you are reading this after that
date, try to find an up-to-date version in the official source repository.

## Supported Versions

Only the latest version of the project is supported with security updates.

## Reporting a Vulnerability

To report a security issue, either:

1. Report it as a [confidential issue], or
1. Send an email to [security@ericcornelissen.dev] with the terms "SECURITY" and "cont-flow-expr"
   in the subject line.

Please do not open a regular issue or Merge Request in the public repository.

To report a security issue in an old version of the project or the development head - i.e. if the
latest release isn't affected, please report it publicly. For example, as a regular issue in the
public repository. If in doubt, report the issue privately.

[confidential issue]: https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html
[security@ericcornelissen.dev]: mailto:security@ericcornelissen.dev?subject=SECURITY%20%28cont-flow-expr%29

### What to Include in a Report

Try to include as many of the following items as possible in a security report:

- An explanation of the issue
- A proof of concept exploit
- A suggested severity
- Relevant [CWE] identifiers
- The latest affected version
- The earliest affected version
- A suggested patch
- An automated regression test

[cwe]: https://cwe.mitre.org/

## Advisories

> **Note**: Advisories will be created only for vulnerabilities present in released versions of the
> project.

| ID               | Date       | Affected versions | Patched versions |
| :--------------- | :--------- | :---------------- | :--------------- |
| -                | -          | -                 | -                |

_This table is ordered most to least recent._

## Acknowledgments

We would like to publicly thank the following reporters:

- _None yet_
