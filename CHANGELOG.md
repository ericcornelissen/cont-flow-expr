# Changelog

All notable changes to `cont-flow-expr` (including sub packages) will be documented in this file.

The format is based on [Keep a Changelog], and this project adheres to [Semantic Versioning].

## [Unreleased]

### `cont-flow-expr`

- _No changes yet._

### `cont-flow-expr.if`

- _No changes yet._

### `cont-flow-expr.switch`

- _No changes yet._

### `cont-flow-expr.try`

- _No changes yet._

## [1.7.1] - 2024-07-05

### `cont-flow-expr`

- Support Node.js v22.

### `cont-flow-expr.if`

- Add JSDoc type definition.
- Support Node.js v22.

### `cont-flow-expr.switch`

- Add JSDoc type definition.
- Support Node.js v22.

### `cont-flow-expr.try`

- Add JSDoc type definition.
- Support Node.js v22.

## [1.7.0] - 2023-08-05

### `cont-flow-expr`

- _Version bump only._

### `cont-flow-expr.if`

- Add lazy condition support.

### `cont-flow-expr.switch`

- Add lazy case support.

### `cont-flow-expr.try`

- _Version bump only._

## [1.6.0] - 2023-07-29

### `cont-flow-expr`

- _Version bump only._

### `cont-flow-expr.if`

- Reduce stack depth for chains that are decided early.

### `cont-flow-expr.switch`

- Add fallthrough support.
- Reduce stack depth for chains with more than 1 case.

### `cont-flow-expr.try`

- _Version bump only._

## [1.5.0] - 2023-07-23

### `cont-flow-expr`

- _Version bump only._

### `cont-flow-expr.if`

- _Version bump only._

### `cont-flow-expr.switch`

- Add multi-valued case support.

### `cont-flow-expr.try`

- _Version bump only._

## [1.4.0] - 2023-07-21

### `cont-flow-expr`

- _Version bump only._

### `cont-flow-expr.if`

- _Initial release._

### `cont-flow-expr.switch`

- _Initial release._

### `cont-flow-expr.try`

- _Initial release._

## [1.3.0] - 2023-07-21

### `cont-flow-expr`

- Add `try-catch` expression as `yrt`.
- Add `cont-flow-expr/try` export.

## [1.2.0] - 2023-07-20

### `cont-flow-expr`

- Add `switch-case` expression as `hctiws`.
- Add `cont-flow-expr/if` export.
- Add `cont-flow-expr/switch` export.

## [1.1.0] - 2023-07-18

### `cont-flow-expr`

- Add lazily evaluated branches support.
- Add TypeScript type declarations.

## [1.0.0] - 2023-07-16

### `cont-flow-expr`

- _Initial release._

[keep a changelog]: https://keepachangelog.com/en/1.0.0/
[semantic versioning]: https://semver.org/spec/v2.0.0.html
