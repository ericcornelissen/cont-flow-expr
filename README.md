# `cont-flow-expr`

Control flow as expression for JavaScript.

## Packages

- [`cont-flow-expr`] - All flow control statements as expressions.
- [`cont-flow-expr.if`] - If statements as expressions.
- [`cont-flow-expr.switch`] - Switch statements as expressions.
- [`cont-flow-expr.try`] - Try statements as expressions.

[`cont-flow-expr`]: ./packages/cont-flow-expr
[`cont-flow-expr.if`]: ./packages/cont-flow-expr.if
[`cont-flow-expr.switch`]: ./packages/cont-flow-expr.switch
[`cont-flow-expr.try`]: ./packages/cont-flow-expr.try

## Motivation

To achieve conditional values in JavaScript you need to either:

- Accept it won't be `const`ant, for example:

  ```javascript
  let value;
  if (condition) {
    value = optionA;
  } else {
    value = optionB;
  }
  ```

  Which is fine if you're okay with it.

- Use the ternary operator, for example:

  ```javascript
  const value = condition ? optionA : optionB;
  ```

  Which is fine but is considered hard to read on its own by some, but definitely becomes harder to
  read with multiple conditions, for example:

  ```javascript
  const value = condition1 ? optionA : condition2 ? optionB : optionC;
  ```

- Put the conditional behind a function call, for example

  ```javascript
  function example(condition) {
    if (condition) {
      return optionA;
    } else {
      return optionB;
    }
  }

  const value = example(condition);
  ```

  Which is fine, and may even make your code more expressive, but the boilerplate involved can
  become cumbersome.

Treating conditionals as expressions instead avoids all of these potential problems. Some languages,
such as [Kotlin] and [Rust], have native support for this. This library aims to bring this language
feature to JavaScript as well, at least in spirit.

[kotlin]: https://kotlinlang.org/docs/control-flow.html
[rust]: https://doc.rust-lang.org/reference/expressions/if-expr.html

## Related

- [`dolla-if`] - Alternative library that does the same as this library.
- [`if-match`] - Advanced multi-condition matching also using expressions.

[`dolla-if`]: https://www.npmjs.com/package/dolla-if
[`if-match`]: https://www.npmjs.com/package/if-match

## License

All source code is licensed under the ISC license, see [LICENSE] for the full license text. All
documentation text is licensed under [CC BY 4.0].

[cc by 4.0]: https://creativecommons.org/licenses/by/4.0/
[license]: ./LICENSE
