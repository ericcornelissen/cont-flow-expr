# Contributing Guidelines

The maintainers of `cont-flow-expr` welcome contributions and corrections. This includes
improvements to the documentation or code base, tests, benchmarks, bug fixes, and implementations of
new features. We recommend you open an issue before making any substantial changes so you can be
sure your work won't be rejected. But for small changes, such as fixing a typo, you can open a Merge
Request directly.

If you decide to make a contribution, please use the following workflow:

- Fork the repository.
- Create a new branch from the latest `main`.
- Make your changes on the new branch.
- Commit to the new branch and push the commit(s).
- Open a Merge Request against `main`.
