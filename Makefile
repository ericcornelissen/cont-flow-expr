CACHE_DIR:=.cache
NODE_MODULES=node_modules
REPORT_DIR:=_reports
TEMP_DIR:=.temp
TEST_FILES:=packages/*/test.js

.PHONY: default
default: help

.PHONY: benchmark
benchmark: $(NODE_MODULES) ## Run benchmarks
	@echo if-else
	@node packages/cont-flow-expr.if/bench.js
	@echo
	@echo switch-case
	@node packages/cont-flow-expr.switch/bench.js
	@echo
	@echo try-catch
	@node packages/cont-flow-expr.try/bench.js

.PHONY: clean
clean: ## Clean the repository
	@git clean -fx \
		$(CACHE_DIR) \
		$(NODE_MODULES) \
		$(REPORT_DIR) \
		$(TEMP_DIR) \
		packages/*/node_modules/ \
		packages/*/CHANGELOG.md \
		packages/*/LICENSE \
		packages/*/SECURITY.md

.PHONY: coverage
coverage: $(NODE_MODULES) ## Run tests with coverage
	@npx c8 \
		--config .c8.json \
		node --test ${TEST_FILES}

.PHONY: help
help: ## Show this help message
	@printf "Usage: make <command>\n\n"
	@printf "Commands:\n"
	@awk -F ':(.*)## ' '/^[a-zA-Z0-9%\\\/_.-]+:(.*)##/ { \
		printf "  \033[36m%-30s\033[0m %s\n", $$1, $$NF \
	}' $(MAKEFILE_LIST)

.PHONY: license-check
license-check: $(NODE_MODULES) ## Check license compliance
	@npx licensee --errors-only

.PHONY: mutation
mutation: $(NODE_MODULES) ## Run mutation tests
	@npx stryker run .stryker.json

.PHONY: publish
publish: ## Publish all packages to npm
	@cd packages/cont-flow-expr.if/ && \
		cp ../../CHANGELOG.md ../../LICENSE ../../SECURITY.md ./ && \
		npm publish --access public --provenance
	@cd packages/cont-flow-expr.switch/ && \
		cp ../../CHANGELOG.md ../../LICENSE ../../SECURITY.md ./ && \
		npm publish --access public --provenance
	@cd packages/cont-flow-expr.try/ && \
		cp ../../CHANGELOG.md ../../LICENSE ../../SECURITY.md ./ && \
		npm publish --access public --provenance
	@cd packages/cont-flow-expr/ && \
		cp ../../CHANGELOG.md ../../LICENSE ../../SECURITY.md ./ && \
		npm publish --access public --provenance

.PHONY: test
test: $(NODE_MODULES) ## Run tests
	@node --test ${TEST_FILES}

.PHONY: verify
verify: license-check coverage mutation ## Verify project is in a good state

$(NODE_MODULES): .npmrc package*.json
	@npm clean-install
